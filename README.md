What is git branch?
===================
A branch in Git is simply a pointer to one specific commit.
A key point to understand is that: when creating a branch - actually 3 branches are involved:
- One branch on a remote machine (named `origin` by default): `master`, `my_feature`
- One branch on the local machine, which is a "copy" of the branch on the remote machine:
  `remotes/origin/master`, `remotes/origin/my_feature`.
  These are called "remote tracking branches".
  So `remotes/origin/master` is a remote tracking branch for `master` in the origin repo.
- One branch on your local machine: e.g. `master`, `my_featue`.

So, on the local machine we usually have a pair of a local branches (`master`, `my_feature`)
and remote tracking branch (`remotes/origin/master`, `remotes/origin/my_feature`).
The local branch is said to be "tracking" its remote branch.
For exampke, `master` branch is a tracking branch for `remotes/origin/master`.
If you’re on a tracking branch and type `git pull`, git automatically knows
which server to fetch from and which branch to merge into.


BitBucket is Web based tool running on our remote machine only (on the "server").


STEP 1: update your remote tracking branches on your local machine (including remotes/origin/master)
====================================================================================================
git fetch [--all]


STEP 2: you can also update your local master by
================================================
git checkout master
git merge FETCH_HEAD

(git pull #git pull = git fetch + git merge)


STEP 2: create your user branch (feature or bugfix)
====================================================
A branch can be created in 2 ways:
1. create it on the server by bitcucket and then make git pull on our loca mahcine
OR
2. create the branch on the local machine (and later pus it to the server)
git branch <new-branch>
note: there many options in git command line for creating a branch
keep in mind that the new branch will point the current commit (HEAD)


STEP 3: developing your feature
================================
git checkout <my_feature>

for every time you an update to source files is ready:
git add ...
git commit ...


during your development, usually master branch is updated on the server by other developers
So, you need to update (align) you feature branch with those changes before updating 
your branch on the server.


STEP 4: UPDATE your local branch:
===================================================
4.1: update your local master branch like in STEP 2:
---------------------------------------------------
git checkout master
git fetch origin master
git merge FETCH_HEAD

(git pull #git pull = git fetch + git merge)


4.2: merge changes from the updated local master into your local feature branch
-------------------------------------------------------------------------------
git checkout my_feature
git rebase master #rebase your local branch on top of the updated master


STEP 5 - PUSH: push you local branch to "server"
=======================================
push your local branch to "server" (if it is already exist on the "server" ,it will update it)
git push origin my_feature:my_feature


MERGE: merge your updated branch on the server into the master branch:
======================================================================
open a Pull Request on BitBucket from your branch to master branch
merge it

TO DO: Handle pull requests comments:
====================================
repeate STEPS 3 to 5
